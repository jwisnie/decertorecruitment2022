import { Pipe, PipeTransform } from "@angular/core";
import { stringNotEmpty } from "../../misc/string-not-empty";

@Pipe({ name: "stringNotEmpty" })
export class StringNotEmptyPipe implements PipeTransform {

    transform(value: any, ...args: any[]): any {
        return stringNotEmpty(value);
    }
}