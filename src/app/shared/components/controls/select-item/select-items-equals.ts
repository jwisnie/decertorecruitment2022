import { SelectItem } from "./select-item.i";

export function selectItemsEquals(a?: SelectItem | null, b?: SelectItem | null): boolean {
    return a === b || (a !== undefined && b !== undefined && a !== null && b !== null && a.id === b.id);
}