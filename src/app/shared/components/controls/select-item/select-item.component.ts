import { Component, forwardRef, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { SelectItem } from './select-item.i';
import { selectItemsEquals } from './select-items-equals';

@Component({
  selector: 'app-select-item',
  templateUrl: './select-item.component.html',
  styleUrls: ['./select-item.component.scss'],
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => SelectItemComponent), multi: true }
  ]
})
export class SelectItemComponent implements ControlValueAccessor {

  @Input()
  items: SelectItem[] = []

  _value?: SelectItem;

  @Input()
  disabled = false;


  onChange?: (value?: SelectItem) => void;
  onTouched?: (value?: SelectItem) => void;


  @Input()
  setValue(value: SelectItem | undefined): boolean {
    if (!selectItemsEquals(this.value, value)) {
      this._value = value;
      if (this.onChange) {
        this.onChange(value);
      }

      if (this.onTouched) {
        this.onTouched(value);
      }

      return true;
    } else {
      return false;
    }
  }

  get value(): SelectItem | undefined {
    return this._value;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(disabled: boolean) {
    this.disabled = disabled;
  }

  writeValue(obj: any): void {
    this.setValue(obj);
  }

  isSelected(value: SelectItem | undefined): boolean {
    return selectItemsEquals(this.value, value);
  }
}
