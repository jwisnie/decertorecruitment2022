export interface SelectItem{
    id: string;
    title: string;
    description?: string;
}