import { SelectItem } from "./select-item.i";
import { selectItemsEquals } from "./select-items-equals";


describe('selectItemsEquals', () => {

  it('should return true for the same value', () => {
    const a: SelectItem = { id: '1', title: 'title' }
    expect(selectItemsEquals(a, a)).toBeTruthy();
  });

  it('should return true for the same id', () => {
    const a: SelectItem = { id: '1', title: 'title1' }
    const b: SelectItem = { id: '1', title: 'title2' }
    expect(selectItemsEquals(a, b)).toBeTruthy();
  });

  it('should return true for undefined', () => {
    expect(selectItemsEquals(undefined, undefined)).toBeTruthy();
  });

  it('should return false for different object', () => {
    const a: SelectItem = { id: '1', title: 'title1' }
    const b: SelectItem = { id: '2', title: 'title2' }
    expect(selectItemsEquals(a, b)).toBeFalsy();
  });

  it('should return false for an object and undefined', () => {
    const a: SelectItem = { id: '1', title: 'title1' }
    expect(selectItemsEquals(a, undefined)).toBeFalsy();
  });

  it('should return false for an undefined and object', () => {
    const a: SelectItem = { id: '1', title: 'title1' }
    expect(selectItemsEquals(undefined, a)).toBeFalsy();
  });
});
