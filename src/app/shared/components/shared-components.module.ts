import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { PanelComponent } from './layout/panel/panel.component';
import { SelectItemComponent } from './controls/select-item/select-item.component';
import { StringNotEmptyPipe } from "./pipes/string-not-empty.pipe";
import { HintComponent } from './layout/hint/hint.component';


const components = [
    PanelComponent,
    SelectItemComponent,
    StringNotEmptyPipe,
    HintComponent
];

@NgModule({
    imports: [CommonModule, FormsModule, ReactiveFormsModule],
    exports: [CommonModule, FormsModule, ReactiveFormsModule, components],
    declarations: [components]

})
export class SharedComponentsModule {

}