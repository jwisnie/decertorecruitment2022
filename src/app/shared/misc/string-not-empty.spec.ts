import { stringNotEmpty } from './string-not-empty';

describe('stringNotEmpty', () => {

  it('should return false for undefined', () => {
    expect(stringNotEmpty(undefined)).toBeFalsy();
  });

  it('should return false for empty string', () => {
    expect(stringNotEmpty('')).toBeFalsy();
  });

  it('should return true for not empty string', () => {
    expect(stringNotEmpty('as')).toBeTruthy();
  });
});
