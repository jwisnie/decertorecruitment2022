export function stringNotEmpty(input?: string) {
    return input !== undefined && input != '';
}