import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { SelectItem } from './shared/components/controls/select-item/select-item.i';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  items: SelectItem[] = [
    { id: "1", title: 'Product 1', description: 'Opis productu 1' },
    { id: "2", title: 'Product 2' },
    { id: "3", title: 'Product 3', description: 'Opis productu 3' },
    { id: "4", title: 'Product 4', description: 'Opis productu 4' },
    { id: "5", title: 'Product 5', description: 'Opis productu 5' },
    { id: "6", title: 'Product 6', description: 'Opis productu 6' }
  ]

  selectedProductFC: FormControl = new FormControl();

  ngOnInit(): void {
    this.selectedProductFC.setValue({id:"4"});
    this.selectedProductFC.valueChanges.subscribe((value: SelectItem) => console.log(value));
  }
}
